import React from "react";
import "./App.css";
import Header from "./components/common/header";
import Home from "./pages";

const App: React.FC = () => {
  return (
    <div className="container">
      <Header />
      <Home />
    </div>
  );
};

export default App;
