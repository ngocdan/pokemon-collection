import React from "react";
import LOGO from "../../assets/logo.png";
import "../../styles/components/header.scss";

const Header = () => {
  return (
    <header className="pokemon-header">
      <img src={LOGO} alt="logo" />
    </header>
  );
};

export default Header;
