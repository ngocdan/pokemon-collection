import React, { useEffect, useState } from "react";
import { getListPokemon, getNextUrl, getPokemonName } from "../api/homeApi";
import { Pokemon, Pokemons } from "../interfaces/home";
import PokemonCollection from "./components/PokemonCollection";
import "../styles/pages/index.scss";

const Home = () => {
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [nextUrl, setNextUrl] = useState<string>("");
  useEffect(() => {
    try {
      const getPokemon = async () => {
        const res = await getListPokemon();
        setNextUrl(res.data.next);
        res.data?.results?.forEach(async (pokemon: Pokemons) => {
          const pokeItem = await getPokemonName(pokemon?.name);
          setPokemons((prev) => [...prev, pokeItem.data]);
        });
      };
      getPokemon();
    } catch (error) {
      console.log(error);
    }
  }, []);

  const nextPage = async () => {
    let res = await getNextUrl(nextUrl);
    setNextUrl(res.data.next);
    res.data?.results?.forEach(async (pokemon: Pokemons) => {
      const pokeItem = await getPokemonName(pokemon?.name);
      setPokemons((prev) => [...prev, pokeItem.data]);
    });
  };

  return (
    <div>
      <PokemonCollection pokemons={pokemons} />
      <div className="btn">
        <button onClick={nextPage}>Load more</button>
      </div>
    </div>
  );
};

export default Home;
