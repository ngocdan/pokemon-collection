import React from "react";

interface Props {
  name: string;
  id: number;
  image: string;
}

const PokemonList = ({ name, id, image }: Props) => {
  return (
    <div>
      <section className="pokemon-list-container">
        <img src={image} alt="pokemon" />
        <p className="pokemon-name">{name}</p>
      </section>
    </div>
  );
};

export default PokemonList;
