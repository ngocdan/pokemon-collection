import React from "react";
import { Pokemon } from "../../interfaces/home";
import PokemonList from "./PokemonList";

interface Props {
  pokemons: Pokemon[];
}

const PokemonCollection = ({ pokemons }: Props) => {
  return (
    <div>
      <section className="collection-container">
        {pokemons.map((pokemon) => (
          <div key={`pokemon-${pokemon.id}`}>
            <PokemonList
              name={pokemon.name}
              id={pokemon.id}
              image={pokemon.sprites.front_default}
            />
          </div>
        ))}
      </section>
    </div>
  );
};

export default PokemonCollection;
