import axios from "axios";
import qs from "qs";

export const getListPokemon = () => {
  return axios({
    method: "GET",
    url: "https://pokeapi.co/api/v2/pokemon?limit=20&offset=20",
  })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
};

export const getPokemonName = (params: string) => {
  return axios({
    method: "GET",
    url: `https://pokeapi.co/api/v2/pokemon/${params}`,
    params: params,
    paramsSerializer: (params) =>
      qs.stringify(params, { arrayFormat: "repeat" }),
  })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
};
export const getNextUrl = (params: string) => {
  return axios({
    method: "GET",
    url: `${params}`,
    params: params,
    paramsSerializer: (params) =>
      qs.stringify(params, { arrayFormat: "repeat" }),
  })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
};
